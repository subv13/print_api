from django.db import models
from django.contrib.postgres.fields import JSONField

from checks.constants import CheckType, CheckStatus


class Printer(models.Model):
    name = models.CharField(verbose_name='Название', max_length=255)
    api_key = models.CharField(verbose_name='Ключ доступа к API', max_length=255)
    check_type = models.CharField(verbose_name='Тип чека', max_length=255, choices=CheckType.CHOICES())
    point_id = models.IntegerField(verbose_name='Точка')


class Check(models.Model):
    printer = models.ForeignKey(verbose_name='Принтер', to=Printer)
    type = models.CharField(verbose_name='Тип чека', max_length=255, choices=CheckType.CHOICES())
    order = JSONField(verbose_name='Заказ')
    status = models.CharField(
        verbose_name='Статус', max_length=255, choices=CheckStatus.CHOICES(), default=CheckStatus.NEW.value
    )
    pdf_file = models.FileField(verbose_name='PDF-файл', upload_to='pdf/')
