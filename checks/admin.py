from django.contrib import admin

from . import models


@admin.register(models.Printer)
class PrinterAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Check)
class CheckAdmin(admin.ModelAdmin):
    pass
