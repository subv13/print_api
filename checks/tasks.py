import base64
import json
import io
import requests

from django.template.loader import get_template
from django.conf import settings

from django_rq import job

from checks.constants import CheckStatus
from checks.models import Check


@job
def render_pdf(check_id):
    check = Check.objects.get(id=check_id)
    rendered_template = get_template(f'{check.type}_check.html') \
        .render(context={'order': check.order})

    data = {
        'contents': base64.b64encode(str(rendered_template).encode('utf-8')).decode(),
    }

    headers = {
        'Content-Type': 'application/json',
    }

    r = requests.post(settings.WKHTMLTOPDF_SERVICE_URL, data=json.dumps(data), headers=headers)

    file_name = '{}_{}.pdf'.format(check.order['id'], check.type)
    check.pdf_file.save(file_name, io.BytesIO(r.content))
    check.status = CheckStatus.RENDERED.value
    check.save()
