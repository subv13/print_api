from enum import Enum


class CheckType(Enum):
    KITCHEN = 'kitchen'
    CLIENT = 'client'

    @classmethod
    def CHOICES(cls):
        return (
            (cls.KITCHEN, 'Для кухни'),
            (cls.CLIENT, 'Для клиента')
        )


class CheckStatus(Enum):
    NEW = 'new'
    RENDERED = 'rendered'
    PRINTED = 'printed'

    @classmethod
    def CHOICES(cls):
        return (
            (cls.NEW, 'Новый'),
            (cls.RENDERED, 'Сгенерирован PDF'),
            (cls.PRINTED, 'Напечатан'),
        )


class CheckFormat(Enum):
    PDF = 'pdf'
    HTML = 'html'
