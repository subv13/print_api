import json
from json import JSONDecodeError
from urllib.parse import urlencode

from django.http.response import JsonResponse, HttpResponse
from django.shortcuts import render_to_response
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

import django_rq

from checks.constants import CheckStatus, CheckFormat, CheckType
from checks.models import Printer, Check
from checks.tasks import render_pdf


@require_POST
@csrf_exempt
def create_checks(request):
    try:
        order = json.loads(request.body)['order']
    except (JSONDecodeError, KeyError):
        return JsonResponse({'error': 'Пустой запрос'}, status=400)

    if Check.objects.filter(order__id=order['id']):
        return JsonResponse({'error': 'Для данного заказа уже созданы чеки'}, status=400)

    point_id = order['point_id']

    printers = Printer.objects.filter(point_id=point_id)

    if not printers:
        return JsonResponse({'error': 'Для данной точки не настроено ни одного принтера'}, status=400)

    checks = [Check(printer=printer, type=printer.check_type, order=order) for printer in printers]

    Check.objects.bulk_create(checks)

    for check in checks:
        django_rq.enqueue(render_pdf, check.id)

    return JsonResponse({'ok': 'Чеки успешно созданы'})


def new_checks(request):
    api_key = request.GET.get('api_key')
    if not api_key:
        return JsonResponse({'error': 'Ошибка авторизации'}, status=401)

    printer = Printer.objects.get(api_key=api_key)
    if not printer:
        return JsonResponse({'error': 'Ошибка авторизации'}, status=401)

    checks = []
    for check in Check.objects.filter(printer=printer, status=CheckStatus.RENDERED.value):
        url_params = {'order_id': check.order['id'], 'type': check.type, 'format': CheckFormat.PDF.value}
        checks.append({'id': check.id, 'url': '{}?{}'.format(reverse('check'), urlencode(url_params))})

    return JsonResponse({'checks': checks})


def check(request):
    api_key = request.GET.get('api_key')
    if not api_key:
        return JsonResponse({'error': 'Ошибка авторизации'}, status=401)

    printer = Printer.objects.get(api_key=api_key)
    if not printer:
        return JsonResponse({'error': 'Ошибка авторизации'}, status=401)

    order_id = request.GET.get('order_id')
    check_type = request.GET.get('type')
    check = Check.objects.get(order__id=int(order_id), type=check_type)
    if not check:
        return JsonResponse({'error': 'Для данного заказа нет чеков'}, status=400)

    check_format = request.GET.get('format')

    if check_format == CheckFormat.HTML.value:
        return render_to_response(f'{check_type}_check.html', {'order': check.order})

    return HttpResponse(check.pdf_file, content_type='application/pdf')


